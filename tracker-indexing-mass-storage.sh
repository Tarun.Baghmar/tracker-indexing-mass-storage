#!/bin/sh

checking_tracker_index(){
  local dir="$1"
  if [ -e "/media/usbstick/Apertis_test/$dir" ]; then
    for f in "/media/usbstick/Apertis_test/$dir"/*
    do 
       echo $(basename $f)
       tracker index --file $f
       sleep 2s
       value_output="`tracker sparql -q "SELECT ?urn WHERE { ?urn nie:url 'file://$f' ; tracker:available true; nie:dataSource 'http://www.tracker-project.org/ontologies/tracker#extractor-data-source'; .}"`"
       echo $value_output
       for word in $value_output ; do
	 case ${word} in
         "None")
	    echo "failed"
            exit 1
            ;;
          esac
       done
    done
  fi

}

checking_tracker_index music
checking_tracker_index videos
checking_tracker_index documents
checking_tracker_index images
